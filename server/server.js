const express = require('express')
const path = require('path')
const dotenv = require('dotenv')
const uuid = require('uuid4')

const app = express()
const port = process.env.PORT || 3000

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.get('/html', function (req, res) {
    res.sendFile(path.join(__dirname,'../client/index.html'))
  })

app.get('/json', function (req, res) {
    res.sendFile(path.join(__dirname,'../client/index.json'))
  })

app.get('/uuid', function (req, res) {
    res.json({'uuid':uuid()})
  })

app.get('/status/:id', function (req, res) {
    res.sendStatus(req.params.id)
  })

app.get('/delay/:id', function (req, res) {
    setTimeout(function(){res.sendStatus(200)},parseInt(req.params.id*1000))
  })

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})